package com.web.service.client;

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import com.selfcare.shared.data.BloodOxygen;
import com.selfcare.shared.data.BloodPressure;
import com.selfcare.shared.data.SelfcareContext;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class BloodOxygenClient {
	
	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		
		insertBloodOxygen();
		//getBloodOxygen();
		//getAllBloodOxygen();
		//updateBloodOxygen();
		//deleteBloodOxygen();

	}

	// insert
	public static void insertBloodOxygen() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen= new BloodOxygen();
		bloodOxygen.setDataId("bloodOxygen99");
		bloodOxygen.setLastChangeTime(new Date());
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/insert");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/insert");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodOxygen bloodOxygen2 =(BloodOxygen)con.get("DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodOxygen2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	//get Data
	public static void getBloodOxygen() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen= new BloodOxygen();
		bloodOxygen.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodOxygen bloodOxygen2 =(BloodOxygen)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodOxygen2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	//getAll Data
	public static void getAllBloodOxygen() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen= new BloodOxygen();
		//weight.setDataId("Weight101");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/queryAll");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/queryAll");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.err.println(con.getList().size());
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// update
	
	public static void updateBloodOxygen() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen=getBloodOxygenData();
		bloodOxygen.setDataSource("data source");
		
		
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/update");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/update");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodOxygen bloodOxygen2 =(BloodOxygen)con.get("DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
	
	
	// delete
	
	public static void deleteBloodOxygen() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen =new BloodOxygen();
		bloodOxygen.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/delete");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/delete");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// for update the exsiting weight data.
	
	public static BloodOxygen getBloodOxygenData() throws JsonGenerationException, JsonMappingException, IOException{
		BloodOxygen bloodOxygen =new BloodOxygen();
		bloodOxygen.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodOxygen);
		selfcareContext.setUri("bloodoxygen/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodoxygen/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			bloodOxygen =(BloodOxygen)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bloodOxygen;	
	}
	
}
