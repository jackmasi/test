package com.web.service.client;

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import com.selfcare.shared.data.BloodPressure;
import com.selfcare.shared.data.FoodIntake;
import com.selfcare.shared.data.SelfcareContext;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class BloodPressureClient {
	
	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		
		//insertBloodPressure();
		getBloodPressure();
		//getAllBloodPressure();
		//updateBloodPressure();
		//deleteBloodPressure();

	}

	// insert
	public static void insertBloodPressure() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure= new BloodPressure();
		bloodPressure.setDataId("bloodPressure99");
		bloodPressure.setLastChangeTime(new Date());
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/insert");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/insert");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodPressure bloodPressure2 =(BloodPressure)con.get("DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodPressure2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	//get Data
	public static void getBloodPressure() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure= new BloodPressure();
		bloodPressure.setDataId("bloodPressure99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodPressure bloodPressure2 =(BloodPressure)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodPressure2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	//getAll Data
	public static void getAllBloodPressure() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure= new BloodPressure();
		//weight.setDataId("Weight101");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/queryAll");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/queryAll");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.err.println(con.getList().size());
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// update
	
	public static void updateBloodPressure() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure=getbloodPressureData();
		bloodPressure.setDataSource("data source");
		
		
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/update");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/update");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodPressure bloodPressure2 =(BloodPressure)con.get("DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
	
	
	// delete
	
	public static void deleteBloodPressure() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure =new BloodPressure();
		bloodPressure.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/delete");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/delete");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// for update the exsiting weight data.
	
	public static BloodPressure getbloodPressureData() throws JsonGenerationException, JsonMappingException, IOException{
		BloodPressure bloodPressure =new BloodPressure();
		bloodPressure.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodPressure);
		selfcareContext.setUri("bloodpressure/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodpressure/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			bloodPressure =(BloodPressure)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bloodPressure;	
	}
	
}
