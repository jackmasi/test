package com.web.service.client;

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import com.selfcare.shared.data.BloodGlucose;
import com.selfcare.shared.data.BloodOxygen;
import com.selfcare.shared.data.SelfcareContext;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class BloodGlucoseClient {
	
	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		
		//insertBloodGlucose();
		getBloodGlucose();
		//getAllBloodGlucose();
		//updateBloodGlucose();
		//deleteBloodGlucose();

	}

	// insert
	public static void insertBloodGlucose() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose= new BloodGlucose();
		bloodGlucose.setDataId("bloodGlucose99");
		bloodGlucose.setLastChangeTime(new Date());
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/insert");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/insert");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodGlucose bloodGlucose2 =(BloodGlucose)con.get("DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodGlucose2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	//get Data
	public static void getBloodGlucose() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose= new BloodGlucose();
		bloodGlucose.setDataId("bloodGlucose99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodGlucose bloodGlucose2 =(BloodGlucose)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
			System.out.println(bloodGlucose2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	//getAll Data
	public static void getAllBloodGlucose() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose= new BloodGlucose();
		//weight.setDataId("Weight101");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/queryAll");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/queryAll");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.err.println(con.getList().size());
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// update
	
	public static void updateBloodGlucose() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose=getBloodGlucoseData();
		bloodGlucose.setDataSource("data source");
		
		
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/update");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/update");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			BloodGlucose bloodGlucose2 =(BloodGlucose)con.get("DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
	
	
	// delete
	
	public static void deleteBloodGlucose() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose =new BloodGlucose();
		bloodGlucose.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/delete");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/delete");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// for update the exsiting weight data.
	
	public static BloodGlucose getBloodGlucoseData() throws JsonGenerationException, JsonMappingException, IOException{
		BloodGlucose bloodGlucose =new BloodGlucose();
		bloodGlucose.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", bloodGlucose);
		selfcareContext.setUri("bloodglucose/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/bloodglucose/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			bloodGlucose =(BloodGlucose)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bloodGlucose;	
	}
	
}
