package com.web.service.client;

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;



import com.selfcare.shared.data.Activity;
import com.selfcare.shared.data.BloodGlucose;
import com.selfcare.shared.data.BloodOxygen;
import com.selfcare.shared.data.SelfcareContext;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class ActivityClient {
	
	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		
		//insertActivity();
		getActivity();
		//getAllActivity();
		//updateActivity();
		//deleteActivity();

	}

	// insert
	public static void insertActivity() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity= new Activity();
		activity.setDataId("activity99");
		activity.setLastChangeTime(new Date());
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/insert");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/insert");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			Activity activity2 =(Activity)con.get("DATA");
			System.out.println(con.getStatusCode());
			System.out.println(activity2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	//get Data
	public static void getActivity() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity= new Activity();
		activity.setDataId("activity99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			Activity activity2 =(Activity)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
			System.out.println(activity2.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	//getAll Data
	public static void getAllActivity() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity= new Activity();
		//weight.setDataId("Weight101");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/queryAll");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/queryAll");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.err.println(con.getList().size());
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// update
	
	public static void updateActivity() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity=getActivityData();
		activity.setDataSource("source");
		
		
		
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/update");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/update");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			Activity activity2 =(Activity)con.get("DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
	
	
	// delete
	
	public static void deleteActivity() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity =new Activity();
		activity.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/delete");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/delete");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	
	// for update the exsiting weight data.
	
	public static Activity getActivityData() throws JsonGenerationException, JsonMappingException, IOException{
		Activity activity =new Activity();
		activity.setDataId("99");
		SelfcareContext selfcareContext= new SelfcareContext();
		selfcareContext.set("DATA", activity);
		selfcareContext.setUri("activity/query");
		
		ClientConfig config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		Client client = Client.create(config);
		WebResource service = client.resource("http://127.0.0.1:8888/rest/activity/query");
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(Feature.FAIL_ON_EMPTY_BEANS);
		String v = objectMapper.writeValueAsString(selfcareContext);
		
		System.err.println(v);
		
		
		try {
			ClientResponse response = service.queryParam("context", v).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, v);
			SelfcareContext con = response.getEntity(SelfcareContext.class);
			activity =(Activity)con.get("QUERY_DATA");
			System.out.println(con.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return activity;	
	}
	
}
